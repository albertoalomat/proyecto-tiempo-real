// buffer_circular.h
////////////////////////

#include <stdint.h>
#define TAM_BUFFER 8


typedef struct{
    uint32_t lec;
    uint32_t crono;
    uint32_t dist;
    uint16_t v;
}fDatos;

void insertar (fDatos msg);
int retirar (fDatos* msg);
int posiciones();
