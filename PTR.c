#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <stdint.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include "buffer_circular.h"


pthread_t t_lector;
pthread_t t_log;

pthread_mutex_t lock;
sem_t huecos, items;

unsigned int longRueda = 0, out = 0;
char filename[37];
FILE* fdlog;

/* TIPO DE DATO: Ya definido en buffer_circular.h
typedef struct{
    uint32_t lec;
    uint32_t crono;
    uint32_t dist;
    uint16_t v;
}fDatos;*/

//######################## PTHREAD LECTOR ########################
void* f_lector(){
    int fd, portok = 0,port = 0;
    struct termios options;
    fDatos datoRX;
    char str[14];

    //Probar distintos dispositivos hasta encontrar en cual está conectado
    while((!portok)&&(port < 20)){
        sprintf(str,"/dev/ttyACM%d",port);
        fd = open(str,O_RDWR | O_NDELAY | O_NOCTTY);
        if(fd < 0) port++;
        else portok = 1;
    }
    if(!portok){
        fprintf(stderr,"No se pudo encontrar ningún dispositivo serie concectado en dev/ttyACM*.\nCompruebe la conexión y vuelva a intentarlo.\n");
        pthread_cancel(t_log);
        pthread_exit(NULL);
    }
    printf("Dispositivo UART localizado en /dev/ttyACM%d.\nConfigure la longitud de rueda para continuar.\n",port);

    //Ajustes del puerto serie
    options.c_cflag = B115200 | CS8;  //Opciones de control
    options.c_iflag = 0; //Opciones de entrada
    options.c_oflag = 0; //Opciones de salida
    options.c_lflag = 0; //Opciones de línea

    //Aplicar cambios
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &options);

    //Recibir datos
    int i = 0;
    char c;
    while(c != 'R') read(fd, &c, 1);    //Espera al primer carácter
    i++;
    while(i < 5){                       //Pasa la etiqueda de RUEDA
        if(read(fd, &c, 1)) i++;
    }
    while(i < 9){                       //Obtener la longitud de la rueda
        if(read(fd, &c, 1)){
            longRueda *= 10;
            longRueda += c - 48;
            i++;
        }
    }

    printf("Longitud de la rueda: %d\n",longRueda); //Mostrar en pantalla el valor obtenido
    i++;

    while(!out){                        //Mientras que no se reciba STOP
        while((c != 'M')&&(c != 'S')) read(fd, &c, 1);  //Esperar al comienzo de la etiqueta
        if(c == 'S') out = 1;                           //Si es S, marcar el final del bucle
        else{
            while(c != 'c') read(fd, &c, 1);    //Espera al marcador de tiempo
            datoRX.crono = 0;                   //Resetear lectura del cronómetro
            while(c != 'd'){                    //Obtener el valor
                if(read(fd, &c, 1)){
                    if(c != 'd'){
                        datoRX.crono *= 10;
                        datoRX.crono += c - 48;
                    }
                }
            }
            datoRX.dist = 0;                    //Resetear lectura de distancia
            while(c != 'v'){                    //Obtener el valor
                if(read(fd, &c, 1)){
                    if(c != 'v'){
                        datoRX.dist *= 10;
                        datoRX.dist += c - 48;
                    }
                }
            }
            datoRX.v = 0;                       //Resetear lectura de velocidad
            while((c != '\r')&&(c != '\n')){    //Obtener el valor
                if(read(fd, &c, 1)){
                    if((c != '\r')&&(c != '\n')){
                        datoRX.v *= 10;
                        datoRX.v += c - 48;
                    }
                }
            }
            sem_wait(&huecos);                  //Esperar un hueco en el buffer circular
            pthread_mutex_lock(&lock);          // # BLOQUEO
            insertar(datoRX);                       //Añadir al buffer circular
            sem_post(&items);
            pthread_mutex_unlock(&lock);        // # DESBLOQUEO
        }
    }
    close(fd);                                  //Cerrar el dispositivo
    printf("Toma de datos finalizada.\nLos datos se han guardado en %s\n\n",filename);
    fflush(stdout);                             //Forzar la escritura de los últimos datos
    while(posiciones()!=0);  //Esperar a que el pthread de log procese todos los datos
    pthread_cancel(t_log);                      //Forzar el apagado del pthread de log
    fflush(fdlog);
    fclose(fdlog);                              //Cerrar el fichero de log
    pthread_exit(NULL);                         //Abandonar thread
}

//######################## PTHREAD de LOG ########################
void* f_log(){
    time_t T = time(NULL);
    struct tm tm = *localtime(&T);  //Obetener fecha y hora
    //Escribir el nombre de log con la fecha y la hora actuales
    sprintf(filename,"BICIBOT_LOG_%04d-%02d-%02d_%02d:%02d:%02d.csv",tm.tm_year + 1900 , tm.tm_mon + 1, tm.tm_mday,tm.tm_hour, tm.tm_min, tm.tm_sec);

    //Abrir el archivo de log
    fdlog = fopen(filename,"w");
    if(fdlog < 0){
        fprintf(stderr,"No se ha podido abrir el archivo %s para escritura\n",filename);
        pthread_exit(NULL);                         //Abandonar thread
    }
    //Escribir cabecera
    fprintf(fdlog,"Tiempo[s];Distancia[m];Velocidad[km/h]\n");
    fDatos datoIN;      //Dato para guardar los valores obtenidos del buffer circular
    while(!out){
        sem_wait(&items);                           //Esperar un dato nuevo
        pthread_mutex_lock(&lock);                  // # BLOQUEO
            retirar(&datoIN);                               //Sacar del buffer circular
            sem_post(&huecos);
        pthread_mutex_unlock(&lock);                // # DESBLOQUEO
        //Guardar valores
        fprintf(fdlog,"%d.%d;%d.%d;%d.%d\n",datoIN.crono/10,datoIN.crono%10, datoIN.dist/1000,(datoIN.dist%1000)/100, datoIN.v/10,datoIN.v%10);
    }
    fflush(fdlog);
    fclose(fdlog);
    pthread_exit(NULL);                         //Abandonar thread
}

int main(){
    //Inicializar semáforos
    sem_init(&huecos,0,TAM_BUFFER);
    sem_init(&items, 0,0);

    //Iniciar threads
    if (pthread_create(&t_lector, NULL, (void *)f_lector,NULL) != 0) {
        fprintf(stderr, "Error al crear thread lector\n");
        exit(2);
    }
    if (pthread_create(&t_log, NULL, (void *)f_log,NULL) != 0) {
        fprintf(stderr, "Error al crear thread log\n");
        exit(2);
    }
    //Esperar a que se unan los pthreads
    pthread_join(t_lector, NULL);
    pthread_join(t_log, NULL);
    return 0;
}
