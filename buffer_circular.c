// Biblioteca para gestionar un buffer circular
//////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include "buffer_circular.h"

static fDatos buffer[TAM_BUFFER];
static int in= 0, out= 0, n= 0; // Indices para gestionar el buffer circular
   //n -> Cuanto de lleno
   //in -> posición de entrada
   //out-> posición de salida

void insertar (fDatos msg)
{
   buffer[in]= msg;  // Si overflow, sobreescribe el msg más antiguo
   in= (in+1)%TAM_BUFFER;
   if (n<TAM_BUFFER) n++;
   else out= (out+1)%TAM_BUFFER;
}

int retirar (fDatos* msg)
{
   if (n==0) {
      return 0;
   }
   else {
      *msg= buffer[out];
      out= (out+1)%TAM_BUFFER;
      n--;
      return 1;
   }
}

int posiciones(){
   return n;
}
