# Proyecto Tiempo Real

## Name
BICIBOT

## Description
The software in this project runs a bicycle computer on an STM32 microcontroller. It has been designed to run on a SMT32 NUCLEO-F411RE development board. Other hardware includes:
- HALL KY-024 Hall Effect Sensor
- 0.96" OLED screen GME12864-41
- 2 pushbuttons with pull-up resistance

## Installation
- File main.c should be implemented in an STMCubeIDE project containing the necessary libraries.
- Files PTR.c and buffer_circular.c should be compiled at any linux system together.

## Usage
Once powered, a message at the OLED screen asks the user to introduce the wheel length. One poshbutton allows the user to increment the selected digit while the other confirms the value. The system starts recording when the configuration has been completed and the hall effect sensor detects the magnetic field. If no wheel turn is detected in 2.8s the system stops until the bike moves again.
The code in PTR allows supervision through the serial port from a linux computer and exporting data as csv log files.

## Support
The software in this project is handed "as is" and no support will be provided.

## Authors
Students at MSc Embedded Systems Engineering at University of the Basque Country (UPV-EHU).
- Alberto Alonso Mata 
- Iván Pérez Gómez

### Third party software
stm32-ssd1306 library by Aleksander Alekseev, under MIT license.
https://github.com/afiskon/stm32-ssd1306

## License
Attribution-NonCommercial-ShareAlike 4.0 International [(CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

