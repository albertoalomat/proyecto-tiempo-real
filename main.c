/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "ssd1306_conf.h"
#include "ssd1306_fonts.h"
#include "ssd1306.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart2;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for taskIC */
osThreadId_t taskICHandle;
const osThreadAttr_t taskIC_attributes = {
  .name = "taskIC",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityRealtime,
};
/* Definitions for taskUART */
osThreadId_t taskUARTHandle;
const osThreadAttr_t taskUART_attributes = {
  .name = "taskUART",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityHigh,
};
/* Definitions for taskCrono */
osThreadId_t taskCronoHandle;
const osThreadAttr_t taskCrono_attributes = {
  .name = "taskCrono",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for colaUART */
osMessageQueueId_t colaUARTHandle;
const osMessageQueueAttr_t colaUART_attributes = {
  .name = "colaUART"
};
/* Definitions for colaIC */
osMessageQueueId_t colaICHandle;
const osMessageQueueAttr_t colaIC_attributes = {
  .name = "colaIC"
};
/* Definitions for estadoPrograma */
osEventFlagsId_t estadoProgramaHandle;
const osEventFlagsAttr_t estadoPrograma_attributes = {
  .name = "estadoPrograma"
};
/* USER CODE BEGIN PV */
typedef struct{
	uint32_t crono;
	uint32_t dist;
	uint16_t v;
}fDatos;
uint16_t circ;
uint32_t crono = 0, lastIC = 0,dist = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM3_Init(void);
static void MX_I2C2_Init(void);
static void MX_TIM4_Init(void);
void StartDefaultTask(void *argument);
void f_taskIC(void *argument);
void f_taskUART(void *argument);
void f_taskCrono(void *argument);

/* USER CODE BEGIN PFP */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
void ssd1306_txt(uint8_t* str, FontDef Font, SSD1306_COLOR color, uint8_t x, uint8_t y, uint8_t r);
void cirRueda();
void loadMainScreen();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM3_Init();
  MX_I2C2_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
  ssd1306_Init();
  ssd1306_Fill(Black);
  ssd1306_txt("BICIBOT",Font_16x26,White,5,17,1);
  HAL_Delay(800);
  cirRueda();				//Obtener circunferencia de la rueda del usuario
  loadMainScreen();			//Cargar pantalla principal
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of colaUART */
  colaUARTHandle = osMessageQueueNew (16, sizeof(fDatos), &colaUART_attributes);

  /* creation of colaIC */
  colaICHandle = osMessageQueueNew (64, sizeof(uint16_t), &colaIC_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of taskIC */
  taskICHandle = osThreadNew(f_taskIC, NULL, &taskIC_attributes);

  /* creation of taskUART */
  taskUARTHandle = osThreadNew(f_taskUART, NULL, &taskUART_attributes);

  /* creation of taskCrono */
  taskCronoHandle = osThreadNew(f_taskCrono, NULL, &taskCrono_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the event(s) */
  /* creation of estadoPrograma */
  estadoProgramaHandle = osEventFlagsNew(&estadoPrograma_attributes);

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */

  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 3600;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 15;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 128;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	uint16_t icval = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
	htim->Instance->CNT = 0;
	lastIC = 0;
	osMessageQueuePut(colaICHandle,&icval,0,0);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == &htim4){
		if(osEventFlagsGet(estadoProgramaHandle)&(0x02)){
			crono++;
			lastIC++;
			if(lastIC > 28)osEventFlagsClear(estadoProgramaHandle, 0x02);	//Bici parada
		}else if((!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13))&&(osEventFlagsGet(estadoProgramaHandle) & 0x01U)){
			osEventFlagsClear(estadoProgramaHandle, 0x01);	//Final del programa
			uint8_t str[] = "STOP\n\r";
			HAL_UART_Transmit(&huart2,str, sizeof(str),1);
		}
	}
}

void ssd1306_txt(uint8_t* str, FontDef Font, SSD1306_COLOR color, uint8_t x, uint8_t y, uint8_t r){
	ssd1306_SetCursor(x,y);
	ssd1306_WriteString(str,Font,color);
	if(r) ssd1306_UpdateScreen();
}

void cirRueda(){
	//2068 - 26"x2.10
	ssd1306_Fill(Black);		//Resetear pantalla
	uint8_t str[8];
	ssd1306_txt("BICIBOT",Font_16x26, White, 5,5,0);
	ssd1306_txt("Circunferencia:",Font_6x8, White, 20,32,0);
	ssd1306_txt("2068mm",Font_6x8, White, 45,45,0);
	uint16_t circ16 = 0x2068;
	uint8_t n = 2;
	ssd1306_Line(45, 55, 50, 55, White);
	ssd1306_UpdateScreen();
	while(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){		//Pulsador de la placa confirma valor (1000)
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8)){
			switch(n){
				case 1: n++;
				break;
				case 2: n--;
				break;
			}
			circ16 = (n << 12) | (circ16 & 0x0FFF);
			sprintf(str,"%xmm",circ16);
			ssd1306_txt(str,Font_6x8, White, 45,45,1);
		}
		HAL_Delay(500);
	}
	ssd1306_Line(45, 55, 50, 55, Black);
	ssd1306_Line(51, 55, 56, 55, White);
	ssd1306_UpdateScreen();
	HAL_Delay(1000);
	n = 0;
	while(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){		//Pulsador de la placa confirma valor (0100)
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8)){
			switch(n){
				case 9: n=0;
				break;
				default: n++;
			}
			circ16 = (n << 8) | (circ16 & 0xF0FF);
			sprintf(str,"%xmm",circ16);
			ssd1306_txt(str,Font_6x8, White, 45,45,1);
		}
		HAL_Delay(500);
	}
	ssd1306_Line(51, 55, 56, 55, Black);
	ssd1306_Line(56, 55, 63, 55, White);
	ssd1306_UpdateScreen();
	HAL_Delay(1000);
	n = 6;
	while(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){		//Pulsador de la placa confirma valor (0010)
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8)){
			switch(n){
				case 9: n=0;
				break;
				default: n++;
			}
			circ16 = (n << 4) | (circ16 & 0xFF0F);
			sprintf(str,"%xmm",circ16);
			ssd1306_txt(str,Font_6x8, White, 45,45,1);
		}
		HAL_Delay(500);
	}
	ssd1306_Line(56, 55, 63, 55, Black);
	ssd1306_Line(64, 55, 69, 55, White);
	ssd1306_UpdateScreen();
	HAL_Delay(1000);
	n = 8;
	while(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13)){		//Pulsador de la placa confirma valor (0001)
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_8)){
			switch(n){
				case 9: n=0;
				break;
				default: n++;
			}
			circ16 = n | (circ16 & 0xFFF0);
			sprintf(str,"%xmm",circ16);
			ssd1306_txt(str,Font_6x8, White, 45,45,1);
		}
		HAL_Delay(500);
	}
	circ = ((circ16 & 0xF000) >> 12) * 1000;
	circ += ((circ16 & 0x0F00) >> 8) * 100;
	circ += ((circ16 & 0x00F0) >> 4) * 10;
	circ += circ16 & 0x000F;
	sprintf(str,"RUEDA%d\r\n");
	HAL_UART_Transmit(&huart2,str,strlen(str),1);
}

void loadMainScreen(){
	ssd1306_Fill(Black);
	ssd1306_txt("00",Font_16x26, White, 82,0,0);
	ssd1306_txt(".0",Font_7x10, White, 114,0,0);
	ssd1306_txt("km",Font_6x8, White, 116,10,0);
	ssd1306_txt("/h",Font_6x8, White, 116,18,0);
	ssd1306_txt("00:00:00",Font_7x10, White, 0,0,0);
	ssd1306_txt("VMedia:",Font_6x8, White, 0,12,0);
	ssd1306_txt("00.0km/h",Font_7x10, White, 0,22,0);
	ssd1306_txt("000.00km",Font_11x18, White, 0,40,1);
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_f_taskIC */
/**
* @brief Function implementing the taskIC thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_taskIC */
void f_taskIC(void *argument)
{
  /* USER CODE BEGIN f_taskIC */
	uint16_t iclist[4];
	dist = 0;
	uint8_t lecturas = 0;
	uint8_t str[8];
	float newbal;
	osDelay(200);
	while(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13)); //Asegurarse de que B1 no está pulsado
	HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_2);
	osEventFlagsSet(estadoProgramaHandle, 0x01);	//Inicio del programa
  /* Infinite loop */
  for(;;)
  {
	  osEventFlagsWait(estadoProgramaHandle,0x01U,osFlagsNoClear,osWaitForever);
	  uint16_t icval;
	  fDatos datosM;
	  osMessageQueueGet(colaICHandle,&icval,0, osWaitForever);
	  osEventFlagsSet(estadoProgramaHandle,0x02);
	  datosM.crono = crono;
	  iclist[3] = iclist[2];
	  iclist[2] = iclist[1];
	  iclist[1] = iclist[0];
	  iclist[0] = icval;
	  if(lecturas < 4){
		  lecturas ++;
		  newbal = (float) icval;
	  }else{
		  newbal = 0.5*iclist[0] + 0.25*iclist[1] + 0.15*iclist[2] + 0.1*iclist[3];
	  }
	  float vf = 840*circ/newbal;
	  datosM.v  = (unsigned int) vf;
	  sprintf(str,"%.2d",datosM.v/10);
	  ssd1306_txt(str,Font_16x26, White, 82,0,0);
	  sprintf(str,"%.1d",datosM.v%10);
	  ssd1306_txt(str,Font_7x10, White, 121,0,0);

	  dist += circ;
	  datosM.dist = dist;
	  uint32_t a = (datosM.dist%1000000)/10000;
	  uint32_t b = datosM.dist/1000000;
	  sprintf(str,"%.3d.%.2d",b,a);
	  ssd1306_txt(str,Font_11x18, White, 0,40,1);


	  osMessageQueuePut(colaUARTHandle,&datosM,0,0);
    osDelay(1);
  }
  /* USER CODE END f_taskIC */
}

/* USER CODE BEGIN Header_f_taskUART */
/**
* @brief Function implementing the taskUART thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_taskUART */
void f_taskUART(void *argument)
{
  /* USER CODE BEGIN f_taskUART */
  /* Infinite loop */
  for(;;)
  {
	  osEventFlagsWait(estadoProgramaHandle,0x01U,osFlagsNoClear,osWaitForever);
	  fDatos datosM;
	  uint8_t str[30];
	  osMessageQueueGet(colaUARTHandle,&datosM,0, osWaitForever);
	  sprintf(str,"Mc%dd%dv%d\r\n",datosM.crono,datosM.dist,datosM.v);
	  HAL_UART_Transmit_IT(&huart2,str,strlen(str));
	  osDelay(1);
  }
  /* USER CODE END f_taskUART */
}

/* USER CODE BEGIN Header_f_taskCrono */
/**
* @brief Function implementing the taskCrono thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_taskCrono */
void f_taskCrono(void *argument)
{
  /* USER CODE BEGIN f_taskCrono */
	HAL_TIM_Base_Start_IT(&htim4);
	uint32_t p_start = 0, p_finish = 0;
  /* Infinite loop */
  for(;;)
  {
	osEventFlagsWait(estadoProgramaHandle,0x01U,osFlagsNoClear,osWaitForever);
	uint32_t h = crono/36000;
	uint32_t m = (crono%36000)/600;
	uint32_t s = (crono%600)/10;
	uint32_t vm = 0;
	if(crono > 0) vm = dist*36/crono/100;
	uint8_t str[9];
	sprintf(str,"%.2d:%.2d:%.2d",h,m,s);
	ssd1306_txt(str,Font_7x10, White, 0,0,0);
	sprintf(str,"%.2d.%.1d",vm/10,vm%10);
	ssd1306_txt(str,Font_7x10, White, 0,22,1);
	ssd1306_DrawArc(105,45, 14, p_start, p_finish , Black);
	p_start = (crono%150)*12/5;
	p_finish = p_start + 300;
	//if(p_start >= )
	ssd1306_DrawArc(105,45, 14, p_start, p_finish , White);
	ssd1306_UpdateScreen();
    osDelay(400);
  }
  /* USER CODE END f_taskCrono */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
